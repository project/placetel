<?php

function placetel_routing() {
  $destination = drupal_get_destination();
  $routings = db_select('placetel_routing', 'r')
    ->fields('r', array('rid', 'name', 'token'))
    ->orderBy('r.name')
    ->execute()
    ->fetchAll();
  $output = '<ul>';
  foreach ($routings as $routing) {
    $links = array(
      t('edit') => 'edit',
      t('delete') => 'delete',
    );
    if (empty($routing->token)) {
      $links[t('create token')] = 'token/reset';
    }
    else {
      $links[t('reset token')] = 'token/reset';
      $links[t('delete token')] = 'token/delete';
    }
    foreach ($links as $name => $path) {
      $links[$name] = l($name, 'admin/config/system/placetel/routing/' . $routing->rid . '/' . $path, array('query' => $destination));
    }

    $output .= '<li>' . $routing->name . ' (' . implode(', ', $links) . ')</li>';
  }
  $output .= '</ul>';

  return $output;
}

function placetel_routing_edit($form, &$form_state, $rid = 0) {
  if ($rid) {
    $s = db_select('placetel_routing', 'r')
      ->fields('r', array())
      ->condition('r.rid', $rid)
      ->execute()
      ->fetchObject();
    $routing_user = db_select('placetel_routing_user', 'ru')
      ->fields('ru', array('uid'))
      ->condition('ru.rid', $rid)
      ->execute()
      ->fetchAllKeyed(0, 0);
  }
  else {
    $s = (object) array(
      'name' => '',
      'routing' => 1,
      'routing_settings' => 1,
      'voice_prompt_id' => 0,
      'forward_voice_prompt_id' => 0,
      'target1' => '',
      'ringing_time1' => 20,
      'target2' => '',
      'ringing_time2' => 20,
      'target3' => '',
      'ringing_time3' => 20,
      'target4' => '',
      'ringing_time4' => 20,
      'target5' => '',
      'ringing_time5' => 20,
      'routing_plan' => 0,
    );
    $routing_user = array();
  }

  // Collect the option values.
  $prompts = placetel_api_get_prompts(TRUE);
  $routing_plans = placetel_api_get_routing_plans(TRUE);
  $rids = user_roles(TRUE, 'access placetel');
  $uids = db_select('users_roles', 'ur')
    ->fields('ur', array('uid'))
    ->condition('ur.rid', array_keys($rids))
    ->execute()
    ->fetchAllKeyed(0, 0);
  $placetel_user = array();
  foreach ($uids as $uid) {
    $placetel_user[$uid] = format_username(user_load($uid));
  }
  $placetel_ids = placetel_api_get_voip_users(TRUE);

  $form['rid'] = array(
    '#type' => 'hidden',
    '#value' => $rid,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#size' => 20,
    '#max_length' => 60,
    '#default_value' => $s->name,
  );
  $form['routing'] = array(
    '#type' => 'select',
    '#title' => t('Routing'),
    '#options' => array(
      '1' => t('Forward'),
      '2' => t('Fax'),
      '3' => t('Routing Plan'),
      '5' => t('IVR'),
      '6' => t('Konferenz'),
      '7' => t('T38 Fax'),
      '8' => t('Queue'),
      '9' => t('Own Application'),
      '10' => t('Group'),
    ),
    '#default_value' => $s->routing,
  );
  $form['container_forward'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="routing"]' => array('value' => 1),
      )
    ),
  );
  $form['container_forward']['routing_settings'] = array(
    '#type' => 'select',
    '#title' => t('Routing Settings'),
    '#options' => array(
      '1' => t('Direct Forward'),
      '3' => t('Voice Box'),
      '4' => t('Voice Box and hang-up'),
      '5' => t('Voice Box and forward'),
    ),
    '#default_value' => $s->routing_settings,
  );
  $form['container_forward']['voice_prompt_id'] = array(
    '#type' => 'select',
    '#title' => t('Voice Prompt ID'),
    '#options' => $prompts,
    '#default_value' => $s->voice_prompt_id,
    '#states' => array(
      'visible' => array(
        ':input[name="routing_settings"]' => array('value' => 3),
      )
    ),
  );
  $form['container_forward']['forward_voice_prompt_id'] = array(
    '#type' => 'select',
    '#title' => t('Voice Prompt ID'),
    '#options' => $prompts,
    '#default_value' => $s->forward_voice_prompt_id,
    '#states' => array(
      'visible' => array(
        ':input[name="routing_settings"]' => array(
          array('value' => 4),
          array('value' => 5),
        ),
      )
    ),
  );
  $form['container_forward']['container_forward'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="routing_settings"]' => array(
          array('value' => 1),
          array('value' => 5),
        ),
      )
    ),
    '#prefix' => '<table><tr><td>&nbsp;</td><td>IP-User</td><td>Target(s)</td><td>Ringing Time</td></tr>',
    '#suffix' => '</table>',
  );
  for ($i = 1; $i <= 5; $i++) {
    $container = 'container' . $i;
    $form['container_forward']['container_forward'][$container] = array(
      '#type' => 'container',
      '#prefix' => '<tr><td>' . $i . '</td>',
      '#suffix' => '</tr>',
    );
    $key = 'siptarget' . $i;
    $form['container_forward']['container_forward'][$container][$key] = array(
      '#type' => 'select',
      '#options' => $placetel_ids,
      '#default_value' => '-',
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );
    $key = 'target' . $i;
    $form['container_forward']['container_forward'][$container][$key] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#max_length' => 255,
      '#default_value' => $s->$key,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );
    $key = 'ringing_time' . $i;
    $form['container_forward']['container_forward'][$container][$key] = array(
      '#type' => 'textfield',
      '#size' => 4,
      '#max_length' => 4,
      '#default_value' => $s->$key,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );
  }
  $form['container_routing_plan'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="routing"]' => array('value' => 3),
      ),
    ),
  );
  $form['container_routing_plan']['routing_plan'] = array(
    '#type' => 'select',
    '#title' => t('Routing Plan'),
    '#options' => $routing_plans,
    '#default_value' => $s->routing_plan,
  );

  $form['uids'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Visible to the following users'),
    '#multiple' => TRUE,
    '#options' => $placetel_user,
    '#default_value'=> $routing_user,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

function placetel_routing_edit_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $rid = $form_state['values']['rid'];
  $uids = $form_state['values']['uids'];
  unset($form_state['values']['uids']);
  for ($i = 1; $i <= 5; $i++) {
    $sipkey = 'siptarget' . $i;
    $key = 'target' . $i;
    if ($form_state['values'][$sipkey] != '-') {
      if (!empty($form_state['values'][$key])) {
        $form_state['values'][$sipkey] .= ',';
      }
      $form_state['values'][$key] = $form_state['values'][$sipkey] . $form_state['values'][$key];
    }
    unset($form_state['values'][$sipkey]);
  }

  if ($rid) {
    db_merge('placetel_routing')
      ->key(array('rid' => $rid))
      ->fields($form_state['values'])
      ->execute();
  }
  else {
    drupal_write_record('placetel_routing', $form_state['values']);
    $rid = $form_state['values']['rid'];
  }

  db_delete('placetel_routing_user')
    ->condition('rid', $rid)
    ->execute();
  $query = db_insert('placetel_routing_user')->fields(array('uid', 'rid'));
  $i = 0;
  foreach ($uids as $uid) {
    if ($uid) {
      $i++;
      $query->values(array(
        'uid' => $uid,
        'rid' => $rid,
      ));
    }
  }
  if ($i) {
    $query->execute();
  }
}

function placetel_routing_delete($form, &$form_state, $rid = 0) {
  $form['rid'] = array(
    '#type' => 'hidden',
    '#value' => $rid,
  );
  return confirm_form($form, 'Do you want to delete the Placetel routing plan %name?', 'admin/config/system/placetel/routing');
}

function placetel_routing_delete_submit($form, &$form_state) {
  db_delete('placetel_routing')
    ->condition('rid', $form_state['values']['rid'])
    ->execute();
}

function placetel_routing_token_reset($rid) {
  $token = drupal_get_token($rid);
  placetel_routing_token_delete($rid, $token);
}

function placetel_routing_token_delete($rid, $token = '') {
  db_update('placetel_routing')
    ->fields(array('token' => $token))
    ->condition('rid', $rid)
    ->execute();
  drupal_goto();
}

/**
 * Callback to select a specific routing plan. Authentication happens through a token.
 *
 * @param $rid
 *   ID of the routing plan that should be used.
 */
function placetel_routing_activate($uid, $rid) {
  if (empty($_GET['token'])) {
    $msg = t('Missing token');
  }
  else {
    $token = check_plain($_GET['token']);
    $routing = db_select('placetel_routing', 'r')
      ->fields('r', array('rid', 'name', 'token'))
      ->condition('r.rid', $rid)
      ->execute()
      ->fetchObject();
    if (empty($routing)) {
      $msg = t('Unkown routing plan');
    }
    else if ($token !== $routing->token) {
      $msg = t('Wrong token');
    }
    else {
      $routing_link = db_select('placetel_routing_user', 'u')
        ->condition('u.rid', $rid)
        ->condition('u.uid', $uid)
        ->countQuery()
        ->execute()
        ->fetchField();
      if (empty($routing_link)) {
        $msg = t('The routing !name is not available for the given user', array('!name' => $routing->name));
      }
      else {
        $result = placetel_routing_activate_internal($rid, $uid);
        if ($result['status']) {
          $msg = t('Successfully switched your routing to !name', array('!name' => $routing->name));
        }
        else {
          $msg = $result['message'];
        }
      }
    }
  }
  print $msg;
  exit;
}

function placetel_routing_activate_internal($rid, $uid = FALSE) {
  if (!$uid) {
    global $user;
    $uid = $user->uid;
  }
  $result = placetel_api_set_routing_call_forward($rid, $uid);
  if (!empty($result['result']) && $result['result'] < 0) {
    // Routing was not successful
    $status = FALSE;
    $message = $result['descr'];
  }
  else {
    db_update('placetel_sipuid')
      ->fields(array('rid_active' => $rid))
      ->condition('uid', $uid)
      ->execute();
    $status = TRUE;
    $message = 'OK';
  }
  return array(
    'status' => $status,
    'message' => $message,
  );
}