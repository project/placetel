<?php

function placetel_admin_settings($form, &$form_state) {
  $form['placetel_api_url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL',
    '#default_value' => variable_get('placetel_api_url', 'api.placetel.de'),
  );
  $form['placetel_api_key'] = array(
    '#type' => 'textfield',
    '#title' => 'API Key',
    '#default_value' => variable_get('placetel_api_key', ''),
  );
  $form['placetel_selectors'] = array(
    '#type' => 'textarea',
    '#title' => 'CSS Selectors',
    '#default_value' => variable_get('placetel_selectors', ''),
    '#description' => t('Comma separated list of selectors which should be treated as phone numbers.'),
  );
  $form['placetel_default_country_code'] = array(
    '#type' => 'textfield',
    '#title' => 'Default Country Code',
    '#default_value' => variable_get('placetel_default_country_code', '49'),
    '#description' => t('Use the two or three digits (e.g. 49 for Germany) without a leading plus symbol and without leading zeros.')
  );

  $form['#validate'][] = 'placetel_admin_settings_validate';
  return system_settings_form($form);
}

function placetel_admin_settings_validate($form, &$form_state) {
  $test = _placetel_submit('test');
  /*
   * 'result'
   * 'result-code'
   * 'descr'
   */
}
