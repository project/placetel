(function ($) {

  Drupal.placetel = Drupal.placetel || {};
  Drupal.settings.placetel = Drupal.settings.placetel || {};

  Drupal.behaviors.placetel = {
    attach: function (context) {
      if (Drupal.settings.placetel.urlAjax === undefined) {
        return;
      }
      $('#placetel-toolbar .routings .routing:not(.placetel-processed)').each(function() {
        $(this).click(function() {
          var container = $(this);
          var name = $(this).html();
          var target = $(this).attr('rid');
          $(this).html('Routing ...');
          $.ajax({
            url: Drupal.settings.placetel.urlAjax,
            async: true,
            global: false,
            type: 'POST',
            data: ({
              type: 'route',
              target: target
            }),
            dataType: 'json',
            complete: function (response) {
              var result = $.parseJSON(response.response);
              $(container).html(name);
              if (result.status) {
                $('#placetel-toolbar .routings .routing').removeClass('active');
                $(container).addClass('active');
              }
              else {
                alert(result.message);
              }
            }
          });
        });
      });
      $(Drupal.settings.placetel.selectors).each(function() {
        if ($(this).hasClass('placetel-processed')) {
          return;
        }
        $(this).addClass('placetel-processed');
        $(this).addClass('placetel-phone');
        $(this).click(function() {
          var container = $(this);
          var target = $(this).html();
          $(this).html('Dialing ...');
          $.ajax({
            url: Drupal.settings.placetel.urlAjax,
            async: true,
            global: false,
            type: 'POST',
            data: ({
              type: 'call',
              target: target
            }),
            dataType: 'html',
            complete: function (response) {
              $(container).html(target);
            }
          });
        });
      });
      $('#placetel-toolbar .routings .bookmark-action:not(.placetel-processed)')
        .addClass('placetel-processed')
        .click(function() {
          var href = $(this).attr('href');
          var name = $(this).attr('name');
          $('#placetel-toolbar .routings .bookmark-placeholder')
            .html(name + ': ' + href)
            .show();
          return true;
        });
    }
  };

  Drupal.Nodejs.callbacks.incomingcall = {
    callback: function (message) {
      console.log(message);
      if (message.callback == 'incomingcall') {
        var info = '';
        if (message.data.link) {
          info = message.data.link;
        }
        $.jGrowl(
          'Caller: '+message.data.caller + info,
          {header: 'Incoming call for '+message.data.recipient+': '+message.data.timestamp, sticky:true}
        );
      }
    }
  };

})(jQuery);